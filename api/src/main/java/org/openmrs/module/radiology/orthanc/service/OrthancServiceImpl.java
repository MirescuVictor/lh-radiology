/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.orthanc.service;

import java.util.Base64;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.module.radiology.RadiologyProperties;
import org.openmrs.module.radiology.orthanc.exception.OrthancException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class OrthancServiceImpl implements OrthancService {

	private final Log log = LogFactory.getLog(this.getClass());

	private static final String uploadPath = "/tools/create-dicom";

	@Autowired
	private RadiologyProperties radiologyProperties;

	@Override
	public String uploadFileToOrthanc(String identifier, String tags, byte[] bytes) throws OrthancException {

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = null;

		try {
			HttpHeaders headers = new HttpHeaders();

			byte creds[] = (radiologyProperties.getOrthancUsername() + ":" + radiologyProperties.getOrthancPassword())
					.getBytes();
			headers.add("Authorization", "Basic " + Base64.getEncoder().encodeToString(creds));
			headers.add("Content-Type", "application/json");

			String url = radiologyProperties.getOrthancURL() + uploadPath;

			log.debug("Uploading File to Orthanc Server for Study - " + identifier + ", URL - " + url);
			HttpEntity<String> request = new HttpEntity<String>(generateJSONRequest(identifier, bytes, tags), headers);

			response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
		} catch (Exception e) {
			log.error(e);
			throw new OrthancException(e.getMessage());
		}

		return response.getBody();
	}

	protected String generateJSONRequest(String identifier, byte[] bytes, String tags) {
		String base64File = Base64.getEncoder().encodeToString(bytes);
		return "{\"Parent\": \"" + identifier + "\", \"Tags\" : " + tags
				+ ", \"Content\" : \"data:application/pdf;base64," + base64File + "\"}";

	}
}